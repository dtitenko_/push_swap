/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:37:14 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:37:14 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/stack.h"

void	rotate_stack(t_stack *p_stack)
{
	int			tmp;
	long long	i;

	if (!p_stack || !(p_stack->arr) || (p_stack->top < 0))
		return ;
	i = p_stack->top;
	tmp = p_stack->arr[p_stack->top];
	while (i--)
		p_stack->arr[i + 1] = p_stack->arr[i];
	p_stack->arr[i + 1] = tmp;
}

void	rrotate_stack(t_stack *p_stack)
{
	int			tmp;
	long long	i;

	if (!p_stack || !(p_stack->arr) || (p_stack->top < 0))
		return ;
	i = 0;
	tmp = p_stack->arr[i];
	while (i++ < p_stack->top)
		p_stack->arr[i - 1] = p_stack->arr[i];
	p_stack->arr[i - 1] = tmp;
}
