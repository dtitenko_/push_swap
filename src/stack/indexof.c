/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   indexof.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:36:59 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:37:00 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/stack.h"

int		ft_stack_indexof(int val, t_stack *p_stack)
{
	int	i;

	i = p_stack->top + 1;
	while (i > 0)
		if (p_stack->arr[--i] == val)
			return (i);
	return (i - 1);
}
