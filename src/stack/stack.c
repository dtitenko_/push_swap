/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:37:22 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:37:24 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/push_swap.h"
#define ISINT(x) (LONG_MAX >= x && x >= LONG_MIN)

t_stack	*ft_fill_stack(int argc, char **argv, int i, t_env **pp_env)
{
	t_stack	*a;
	int		len;
	int		j;
	long	num;

	len = argc - i;
	j = argc;
	if (!(a = ft_init_stack(len, 'a')))
		return (NULL);
	while (--j >= i)
	{
		num = ft_atol_base(argv[j], 10);
		if (!ISINT(num) || instack(a, (int)num) || (!ft_isallnum(argv[j])
			&& !(argv[j][0] == '-' && ft_isallnum(&argv[j][1]))))
		{
			ft_free_stack(&a);
			ft_print_error(pp_env);
		}
		push(a, (int)num);
	}
	return (a);
}

t_stack	*ft_init_stack(int maxlen, char ch)
{
	t_stack *stack;

	if (!(stack = (t_stack*)malloc(sizeof(t_stack))))
		return (NULL);
	stack->maxlen = maxlen;
	stack->top = -1;
	stack->ch = ch;
	if (!(stack->arr = (int *)malloc(sizeof(int) * maxlen)))
	{
		ft_free_stack(&stack);
		return (NULL);
	}
	ft_bzero(stack->arr, stack->maxlen * sizeof(int));
	return (stack);
}

/*
** free stack pointer and set it to NULL
** @param pp_stack - adress to pointer to stack var
*/

void	ft_free_stack(t_stack **pp_stack)
{
	if (!(pp_stack && *pp_stack))
		return ;
	if (((*pp_stack)->arr))
		ft_memdel((void **)&((*pp_stack)->arr));
	ft_memdel((void **)pp_stack);
}
