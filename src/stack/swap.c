/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:37:26 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:37:28 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/stack.h"

void	swap_stack(t_stack *p_stack)
{
	int tmp;

	if (p_stack->top < 1)
		return ;
	tmp = p_stack->arr[p_stack->top];
	p_stack->arr[p_stack->top] = p_stack->arr[p_stack->top - 1];
	p_stack->arr[p_stack->top - 1] = tmp;
}
