/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell_sort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:37:17 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:37:18 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#define CHCK_SORT (desc ? (arr[j - k] <= tmp) : (arr[j - k] > tmp))

void	shell_sort(int *arr, size_t size, int desc)
{
	size_t	i;
	size_t	j;
	size_t	k;
	int		tmp;

	k = size / 2;
	while ((k /= 2) > 0)
	{
		i = k;
		while (i++ < size)
		{
			tmp = arr[i - 1];
			j = i - 1;
			while (j >= k)
			{
				if (CHCK_SORT)
					arr[j] = arr[j - k];
				else
					break ;
				j -= k;
			}
			arr[j] = tmp;
		}
	}
}
