/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   min_max.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:37:05 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:37:06 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/stack.h"

static int	ft_m_in_stack(t_stack *p_stack, int max)
{
	int		i;
	int		m;
	int		ch;

	i = p_stack->top;
	m = p_stack->arr[i];
	while (i--)
	{
		ch = max ? m < p_stack->arr[i] : m > p_stack->arr[i];
		m = ch ? p_stack->arr[i] : m;
	}
	return (m);
}

int			ft_max_in_stack(t_stack *p_stack)
{
	return (ft_m_in_stack(p_stack, 1));
}

int			ft_min_in_stack(t_stack *p_stack)
{
	return (ft_m_in_stack(p_stack, 0));
}
