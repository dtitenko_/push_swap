/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   med.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:37:02 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:37:03 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/stack.h"
#include "libft/includes/libft.h"

#define SIZE ((size_t)(sz))
#define MED (sz % 2 ? arr[sz / 2 - 1] : (arr[sz / 2] + arr[sz / 2 - 1]) / 2)

int	ft_med_in_stack(t_stack *p_stack, int sz)
{
	int	*arr;
	int	med;
	int	i;

	if (!(arr = (int *)malloc(SIZE * sizeof(int))))
		return (0);
	med = p_stack->top - sz;
	i = -1;
	while (++i < sz)
		arr[i] = p_stack->arr[++med];
	shell_sort(arr, SIZE, 0);
	med = MED;
	ft_memdel((void **)&arr);
	return (med);
}
