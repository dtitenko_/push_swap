/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   order.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:37:08 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:37:08 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/stack.h"

int	ft_check_order(t_stack *p_stack, int desc)
{
	int i;
	int rorder;

	i = p_stack->top;
	while (i--)
	{
		rorder = (desc ? p_stack->arr[i] < p_stack->arr[i + 1]
						: p_stack->arr[i] > p_stack->arr[i + 1]);
		if (!rorder)
			return (0);
	}
	return (1);
}
