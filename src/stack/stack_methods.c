/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_methods.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:37:20 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:37:21 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/push_swap.h"

int			isfull(t_stack *p_stack)
{
	return (p_stack->top == p_stack->maxlen - 1);
}

int			isempty(t_stack *p_stack)
{
	return (p_stack->top == -1);
}

int			instack(t_stack *p_stack, int val)
{
	int	i;

	i = p_stack->top;
	while (i >= 0)
		if (p_stack->arr[i--] == val)
			return (1);
	return (0);
}

long long	push(t_stack *p_stack, int data)
{
	if (isfull(p_stack))
		return (0);
	p_stack->top++;
	p_stack->arr[p_stack->top] = data;
	return (p_stack->top - p_stack->maxlen - 1);
}

long long	pop(t_stack *p_stack, int *data)
{
	if (isempty(p_stack))
		return (0);
	(*data) = p_stack->arr[p_stack->top];
	return (p_stack->top-- + 1);
}
