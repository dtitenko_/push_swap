/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helpers.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:36:15 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:36:16 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/stack.h"
#include "ft_printf/includes/ft_printf.h"
#include "includes/env.h"

#define IS_RRA ft_strequ(buf + i - 5, "\nrra\n")
#define IS_RRB ft_strequ(buf + i - 5, "\nrrb\n")
#define IS_RA ft_strequ(buf + i - 4, "\nra\n")
#define IS_RB ft_strequ(buf + i - 4, "\nrb\n")
#define IS_SA ft_strequ(buf + i - 4, "\nsa\n")
#define IS_SB ft_strequ(buf + i - 4, "\nsb\n")
#define IS_PA ft_strequ(buf + i - 4, "\npa\n")
#define IS_PB ft_strequ(buf + i - 4, "\npb\n")

#define CMD_IS_RRA ft_strequ(cmd, "rra\n")
#define CMD_IS_RRB ft_strequ(cmd, "rrb\n")
#define CMD_IS_RA ft_strequ(cmd, "ra\n")
#define CMD_IS_RB ft_strequ(cmd, "rb\n")
#define CMD_IS_SA ft_strequ(cmd, "sa\n")
#define CMD_IS_SB ft_strequ(cmd, "sb\n")
#define CMD_IS_PA ft_strequ(cmd, "pa\n")
#define CMD_IS_PB ft_strequ(cmd, "pb\n")

#define P_P (IS_PA && CMD_IS_PB) || (IS_PB && CMD_IS_PA)
#define RR_R (IS_RRA && CMD_IS_RA) || (IS_RRB && CMD_IS_RB)
#define R_RR (IS_RA && CMD_IS_RRA) || (IS_RB && CMD_IS_RRB)
#define S_S (IS_SA && CMD_IS_SA) || (IS_SB && CMD_IS_SB)

#define CHAR2CHANGE *(buf + i - 2)

void	ft_verbose(t_env *p_env, char *cmd)
{
	size_t	len;

	if (!(p_env->flags & F_VERBOSE))
		return ;
	len = ft_strlen(cmd);
	len -= (cmd[len - 1] == '\n' ? 1 : 0);
	p_env->flags & F_COLOR ? ft_printf("Exec {fg}%.*s{eoc}:\n", len, cmd)
							: ft_printf("Exec %.*s:\n", len, cmd);
	print_stacks(p_env->a, p_env->b);
}

int		ft_subst(char *buf, char *cmd)
{
	size_t	i;
	int		r;

	i = ft_strlen(buf);
	r = 0;
	if (i > 4)
	{
		r = (P_P || S_S || R_RR) ? 3 : r;
		r = (RR_R && i > 5) ? 4 : r;
		ft_bzero(buf + i - r, (size_t)r);
		r = (i > 5) && ((IS_RRA && CMD_IS_RRB) || (IS_RRB && CMD_IS_RRA) ||
		(IS_RA && CMD_IS_RB) || (IS_RB && CMD_IS_RA)) ? CHAR2CHANGE = 'r' : r;
		r = (IS_SA && CMD_IS_SB) || (IS_SB && CMD_IS_SA) ?
			(CHAR2CHANGE = 's') : r;
	}
	return (r);
}

void	push2buf(t_env *p_env, char *cmd)
{
	char *old;

	if (p_env->flags & F_VERBOSE)
		ft_verbose(p_env, cmd);
	else if (!(ft_subst(p_env->buf, cmd)))
	{
		old = p_env->buf;
		p_env->buf = ft_strjoin(old, cmd);
		ft_strdel(&old);
	}
}

void	print_stacks(t_stack *a, t_stack *b)
{
	long long int	i;

	i = (a->top >= b->top) ? a->top : b->top;
	while (i-- + 1)
	{
		(i < a->top) ? ft_printf("%11d |%11d| ", i + 1, a->arr[i + 1])
					: ft_printf("%11d |%11c| ", i + 1, ' ');
		(i < b->top) ? ft_printf("|%11d|\n", b->arr[i + 1])
					: ft_printf("|%11c|\n", ' ');
	}
	ft_printf("%11c |-----------| |-----------|\n", ' ');
	ft_printf("%11c |%11c| |%11c|\n\n\n", ' ', a->ch, b->ch);
}
