/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_argv.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:36:18 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:36:19 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/env.h"

int	ft_isallnum(char *str)
{
	if (!str)
		return (0);
	while (*str)
	{
		if (!ft_isdigit(*str))
			return (0);
		str++;
	}
	return (1);
}

int	ft_parse_argv_flag_short(char *argvi)
{
	int flag;

	flag = 0;
	argvi++;
	while (*argvi && ft_strchr("vcrho", *argvi))
	{
		flag |= (*argvi == 'v') ? F_VERBOSE : flag;
		flag |= (*argvi == 'c') ? F_COLOR : flag;
		flag |= (*argvi == 'r') ? F_REV : flag;
		flag |= (*argvi == 'h') ? F_HELP : flag;
		flag |= (*argvi == 'o') ? F_OLDALG : flag;
		argvi++;
	}
	flag |= (*argvi) ? PARSE_ERROR : flag;
	return (flag);
}

int	ft_parse_argv_flag_long(char *argvi)
{
	int	flag;

	flag = 0;
	flag |= (ft_strequ("--verbose", argvi)) ? F_VERBOSE : flag;
	flag |= (ft_strequ("--color", argvi)) ? F_COLOR : flag;
	flag |= (ft_strequ("--reverse", argvi)) ? F_REV : flag;
	flag |= (ft_strequ("--help", argvi)) ? F_HELP : flag;
	flag |= (ft_strequ("--old-algorithm", argvi)) ? F_OLDALG : flag;
	flag |= (!flag) ? PARSE_ERROR : flag;
	return (flag);
}

int	parse_argv(int argc, char **argv, int *stop)
{
	int	flags;
	int	i;

	flags = 0;
	i = 0;
	while (++i < argc)
		if (argv[i][0] == '-' && ft_isalpha(argv[i][1]))
			flags |= ft_parse_argv_flag_short(argv[i]);
		else if (ft_strnequ("--", argv[i], 2) && ft_isalpha(argv[i][3]))
			flags |= ft_parse_argv_flag_long(argv[i]);
		else
			break ;
	(stop) ? *stop = i : 0;
	if (flags & F_HELP)
		ft_usage(argc, argv);
	return (flags);
}
