/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:36:03 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:36:05 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/includes/libft.h"
#include "includes/stack.h"

t_env	*ft_envinit(void)
{
	t_env	*env;

	if (!(env = (t_env *)malloc(sizeof(t_env))))
		return (NULL);
	if (!(env->buf = ft_strnew(0)))
		return (NULL);
	env->a = NULL;
	env->b = NULL;
	env->flags = 0;
	env->fd = 0;
	return (env);
}

void	ft_envdel(t_env **env)
{
	if (env && *env)
	{
		ft_free_stack(&((*env)->a));
		ft_free_stack(&((*env)->b));
		ft_strdel(&((*env)->buf));
		ft_memdel((void **)env);
	}
}
