/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:36:07 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:36:08 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/env.h"

void	ft_print_error(t_env **pp_env)
{
	char *msg;

	msg = (*pp_env)->flags & F_COLOR ? "\033[38;5;1mError\033[0m\n"
									: "Error\n";
	ft_putstr_fd(msg, 2);
	ft_envdel(pp_env);
	exit(0);
}
