/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:36:47 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:36:48 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/push_swap.h"
#define SIZE_A_R !R ? (sz - (sz / 2)) : (sz / 2)
#define SIZE_B_R R ? (sz - (sz / 2)) : (sz / 2)

int		match_med(int med, t_env *p_env, char stack, int sz)
{
	int i;

	i = C_TI;
	while (--sz && --i > -2)
	{
		if ((ONLY_A && CHECK(C_ELEM(i + 1) <= med))
			|| (ONLY_B && CHECK(C_ELEM(i + 1) > med)))
			return (1);
	}
	return (0);
}

void	partition(t_env *p_env, char stack, int sz)
{
	int len;
	int c_len;
	int med;

	len = 0;
	c_len = sz + 1;
	med = ft_med_in_stack(C_STACK, sz);
	while (match_med(med, p_env, stack, c_len) && --c_len)
	{
		if (isempty(STCK(b)) && ft_check_order(STCK(a), R))
			return ;
		if ((ONLY_A && CHECK(C_TOP <= med))
			|| (ONLY_B && CHECK(C_TOP > med)))
			CSTACK_PUSH;
		else if (++len)
			CSTACK_ROT;
	}
	med = (len >= C_STACK->top + 1 - len) | ((STCK(a)->top + 1 != sz) << 1);
	len = med & 0x01 ? C_STACK->top + 1 - len : len;
	while (med & 0x02 && len--)
		med & 0x01 ? CSTACK_ROT : CSTACK_RROT;
	ft_sort(p_env, A, SIZE_A_R);
	ft_sort(p_env, B, SIZE_B_R);
}

void	ft_sort(t_env *p_env, char stack, int sz)
{
	if ((ONLY_A && ft_check_order(STCK(a), R)))
		return ;
	if (ONLY_B && !C_TI)
		CSTACK_PUSH;
	else if (sz == 2)
	{
		((ONLY_A && CHECK(C_TOP > C_N2T)) || ((ONLY_B && CHECK(C_TOP < C_N2T))))
		? CSTACK_SWAP : 0;
		ONLY_B ? CSTACK_PUSH : 0;
		ONLY_B ? CSTACK_PUSH : 0;
	}
	else if ((sz == 3 || sz == 4) && C_TI == sz - 1)
		sz == 3 ? ft_sort3(p_env, stack) : ft_sort4_max(p_env, stack);
	else if ((sz == 3 || (sz == 4 && C_M4TH)) && C_TI >= sz)
	{
		ONLY_A ? ft_sort3_top_a(p_env, stack) : ft_sort3_top_b(p_env, stack);
		sz == 4 && ONLY_B ? CSTACK_PUSH : 0;
	}
	else
		partition(p_env, stack, sz);
}
