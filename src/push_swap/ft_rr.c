/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rr.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:36:34 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:36:35 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/push_swap.h"

void	ft_rra(t_env *p_env)
{
	rrotate_stack(p_env->a);
	push2buf(p_env, "rra\n");
}

void	ft_rrb(t_env *p_env)
{
	rrotate_stack(p_env->b);
	push2buf(p_env, "rrb\n");
}

void	ft_rrr(t_env *p_env)
{
	rrotate_stack(p_env->a);
	rrotate_stack(p_env->b);
	push2buf(p_env, "rrr\n");
}
