/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_s.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:36:38 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:36:39 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/push_swap.h"

void	ft_sa(t_env *p_env)
{
	swap_stack(p_env->a);
	push2buf(p_env, "sa\n");
}

void	ft_sb(t_env *p_env)
{
	swap_stack(p_env->b);
	push2buf(p_env, "sb\n");
}

void	ft_ss(t_env *p_env)
{
	swap_stack(p_env->a);
	swap_stack(p_env->b);
	push2buf(p_env, "ss\n");
}
