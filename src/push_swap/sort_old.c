/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_old.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:36:54 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:36:55 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/push_swap.h"

int		ft_check_n_swap_first(t_env *p_env)
{
	int	cur;

	cur = TOP_INDX(a) + 1;
	if (cur > 3 && CHECK(TOP(a) < NEXT2TOP(a)) && CHECK(TOP(a) < BOTTOM(a))
		&& CHECK(NEXT2TOP(a) > BOTTOM(a)))
		return (0);
	while (--cur)
	{
		if (TOP_INDX(a) == cur && CHECK(TOP(a) > NEXT2TOP(a)))
		{
			ft_sa(p_env);
			if (ft_check_order(p_env->a, R))
				return (1);
		}
		if (TOP_INDX(a) > 2 && cur == 1 && CHECK(PREV2BOTTOM(a) > BOTTOM(a))
			&& ft_check_n_swap_last(p_env))
			return (1);
		if (CHECK(ELEM(a, cur) > ELEM(a, cur - 1)))
			return (0);
	}
	return (!CHECK(TOP(a) < BOTTOM(a)));
}

int		ft_push_all_back2a(t_env *p_env)
{
	if (ft_check_order(p_env->a, R))
	{
		if (TOP_INDX(b) > 0 && ft_check_order(p_env->b, !R))
			while (TOP_INDX(b) > 0)
				ft_pa(p_env);
		if (TOP_INDX(b) == 0)
			ft_pa(p_env);
		return (1);
	}
	return (0);
}

int		ft_check_n_swap_last(t_env *p_env)
{
	ft_rra(p_env);
	if (ft_check_order(p_env->a, R))
		return (1);
	ft_rra(p_env);
	ft_sa(p_env);
	ft_ra(p_env);
	ft_ra(p_env);
	return (ft_check_order(p_env->a, 0));
}

void	ft_sort_old(t_env *p_env)
{
	int		tmp;
	int		cur;
	void	(*f)(t_env *p);

	if (ft_check_n_swap_first(p_env))
		return ;
	cur = TOP_INDX(a) + 1;
	while (--cur)
	{
		tmp = R ? ft_max_in_stack(p_env->a) : ft_min_in_stack(p_env->a);
		f = (ROT2(a) ? ft_ra : ft_rra);
		while (TOP(a) != tmp)
			if ((ft_stack_indexof(tmp, p_env->a) == TOP_INDX(a) - 1))
				ft_sa(p_env);
			else
				f(p_env);
		if (ft_push_all_back2a(p_env))
			return ;
		ft_pb(p_env);
	}
	while (TOP_INDX(b) > -1)
		ft_pa(p_env);
}
