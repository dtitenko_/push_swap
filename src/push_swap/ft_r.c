/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_r.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:36:28 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:36:30 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/push_swap.h"

void	ft_ra(t_env *p_env)
{
	rotate_stack(p_env->a);
	push2buf(p_env, "ra\n");
}

void	ft_rb(t_env *p_env)
{
	rotate_stack(p_env->b);
	push2buf(p_env, "rb\n");
}

void	ft_rr(t_env *p_env)
{
	rotate_stack(p_env->a);
	rotate_stack(p_env->b);
	push2buf(p_env, "rr\n");
}
