/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort3.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:38:18 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:38:19 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/push_swap.h"

#define CHECK_REV(con) REV == !(con)

void	ft_push2a(t_env *p_env, char stack, int n)
{
	while (ONLY_B && n--)
		CSTACK_PUSH;
}

/*
** Sort stack with only 3 elements in it.
*/

void	ft_sort3(t_env *p_env, char stack)
{
	if (C_TBM || C_MTB || C_BMT)
		CSTACK_SWAP;
	if (C_BTM)
		CSTACK_ROT;
	if (C_MBT)
		CSTACK_RROT;
	ft_push2a(p_env, stack, C_TI + 1);
}

void	ft_sort3_top_a(t_env *p_env, char stack)
{
	if (C_BTM || C_BMT)
		CSTACK_SWAP;
	if (C_TBM || C_MBT)
	{
		CSTACK_ROT;
		CSTACK_SWAP;
		CSTACK_RROT;
	}
	if (C_MTB)
		CSTACK_SWAP;
}

void	ft_sort3_top_b(t_env *p_env, char stack)
{
	if (C_BTM || C_MTB || C_TBM || C_TMB)
	{
		(C_BTM || C_MTB) ? CSTACK_SWAP : 0;
		CSTACK_PUSH;
		C_BT ? CSTACK_SWAP : 0;
		CSTACK_PUSH;
		CSTACK_PUSH;
	}
	else if (C_BMT || C_MBT)
	{
		CSTACK_ROT;
		CSTACK_SWAP;
		CSTACK_PUSH;
		if (CHECK_REV(C_BOT < C_TOP))
		{
			CSTACK_RROT;
			CSTACK_PUSH;
		}
		else
		{
			CSTACK_PUSH;
			CSTACK_RROT;
		}
		CSTACK_PUSH;
	}
}

/*
** Sort stack with 4 elements.
*/

void	ft_sort4_max(t_env *p_env, char stack)
{
	int	tmp;
	int tmp1;
	int	rot;

	tmp = REV ? ft_max_in_stack(C_STACK) : ft_min_in_stack(C_STACK);
	tmp1 = !REV ? ft_max_in_stack(C_STACK) : ft_min_in_stack(C_STACK);
	rot = C_ROT2;
	ft_stack_indexof(tmp1, C_STACK) ? 0 :
		IF_A_ELSE_B(ft_sort3_top_a(p_env, stack), ft_sort3_top_b(p_env, stack));
	while (C_TOP != tmp && C_TI)
		if ((C_N2T == tmp) && CHECK_REV(C_TOP < C_BOT))
			CSTACK_SWAP;
		else
			(rot ? CSTACK_ROT : CSTACK_RROT);
	if (!ft_check_order(C_STACK, REV))
	{
		CSTACK_PUSH;
		ft_sort3(p_env, stack);
		ONLY_A ? NCSTACK_PUSH : 0;
	}
	ft_push2a(p_env, stack, C_TI + 1);
}
