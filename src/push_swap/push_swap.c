/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:36:51 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:36:52 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf/includes/ft_printf.h"
#include "includes/push_swap.h"
#define PRINT_STACKS print_stacks(p_env->a, p_env->b)

int		main(int argc, char *argv[])
{
	t_env	*p_env;
	int		i;

	if (!(p_env = ft_envinit()))
		return (0);
	p_env->flags = parse_argv(argc, argv, &i);
	if (p_env->flags & PARSE_ERROR)
		ft_print_error(&p_env);
	if ((argc - i) && !(p_env->flags & F_HELP))
	{
		p_env->a = ft_fill_stack(argc, argv, i, &p_env);
		p_env->b = ft_init_stack(p_env->a->maxlen, 'b');
		p_env->flags & F_VERBOSE ? ft_printf("Init a and b:\n") : 0;
		p_env->flags & F_VERBOSE ? PRINT_STACKS : 0;
		if (TOP_INDX(a) > 0 && TOP_INDX(a) != 2 && CHECK(TOP(a) > NEXT2TOP(a)))
			ft_rra(p_env);
		p_env->flags & F_OLDALG ? ft_sort_old(p_env) :
								ft_sort(p_env, A, p_env->a->maxlen);
		p_env->flags & F_COLOR ? ft_printf("{fg}%s{eoc}", p_env->buf)
							: ft_printf(p_env->buf);
	}
	ft_envdel(&p_env);
	return (0);
}
