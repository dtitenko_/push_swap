/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_p.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:36:23 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:36:24 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/push_swap.h"

void	ft_pa(t_env *p_env)
{
	push_stack(p_env->a, p_env->b);
	push2buf(p_env, "pa\n");
}

void	ft_pb(t_env *p_env)
{
	push_stack(p_env->b, p_env->a);
	push2buf(p_env, "pb\n");
}
