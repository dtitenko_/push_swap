/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:35:47 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:35:53 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/checker.h"

#define ERROR	(-1)

/*
** Function to read, exec commands and check order in stack.
** return:
**     0 - wrong order;
**     1 - right order;
**    -1 - some error has occured;
*/

int		ft_check(t_env *p_env)
{
	char	*cmd;
	int		ret;

	while (1 == (ret = get_next_line(p_env->fd, &cmd)))
	{
		if (!ft_exec(p_env, cmd))
			return (ERROR);
		ft_verbose(p_env, cmd);
		ft_strdel(&cmd);
	}
	ft_strdel(&cmd);
	ret = (-1 == ret ? ERROR : ft_check_order(p_env->a, p_env->flags & F_REV));
	return (ret && (STCK(b)->top == -1));
}
