/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:35:37 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:35:40 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf/includes/ft_printf.h"
#include "includes/checker.h"
#define PRINT_STACKS print_stacks(env->a, env->b)

int	main(int argc, char *argv[])
{
	t_env	*env;
	int		i;
	char	*fmt;

	if (!(env = ft_envinit()))
		return (0);
	env->flags = parse_argv(argc, argv, &i);
	if (env->flags & PARSE_ERROR)
		ft_print_error(&env);
	if ((argc - i) && !(env->flags & F_HELP))
	{
		env->a = ft_fill_stack(argc, argv, i, &env);
		env->b = ft_init_stack(env->a->maxlen, 'b');
		env->flags & F_VERBOSE ? ft_printf("Init a and b:\n") : 0;
		env->flags & F_VERBOSE ? PRINT_STACKS : 0;
		if (-1 == (i = ft_check(env)))
			ft_print_error(&env);
		if (env->flags & F_COLOR)
			fmt = i ? "{fg}%s{eoc}\n" : "{fr}%s{eoc}\n";
		else
			fmt = "%s\n";
		ft_printf(fmt, i ? "OK" : "KO");
	}
	ft_envdel(&env);
	return (0);
}
