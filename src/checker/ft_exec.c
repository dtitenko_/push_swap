/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exec.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:35:57 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:35:58 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/checker.h"

#define ARGS p_env, cmd

int		ft_exec_s(t_env *p_env, char *cmd)
{
	if (ft_strequ("sa", cmd))
		swap_stack(STCK(a));
	else if (ft_strequ("sb", cmd))
		swap_stack(STCK(b));
	else if (ft_strequ("ss", cmd))
	{
		swap_stack(STCK(a));
		swap_stack(STCK(b));
	}
	else
		return (0);
	return (1);
}

int		ft_exec_p(t_env *p_env, char *cmd)
{
	if (ft_strequ("pa", cmd))
		push_stack(STCK(a), STCK(b));
	else if (ft_strequ("pb", cmd))
		push_stack(STCK(b), STCK(a));
	else
		return (0);
	return (1);
}

int		ft_exec_r(t_env *p_env, char *cmd)
{
	if (ft_strequ("ra", cmd))
		rotate_stack(STCK(a));
	else if (ft_strequ("rb", cmd))
		rotate_stack(STCK(b));
	else if (ft_strequ("rr", cmd))
	{
		rotate_stack(STCK(a));
		rotate_stack(STCK(b));
	}
	else
		return (0);
	return (1);
}

int		ft_exec_rr(t_env *p_env, char *cmd)
{
	if (ft_strequ("rra", cmd))
		rrotate_stack(STCK(a));
	else if (ft_strequ("rrb", cmd))
		rrotate_stack(STCK(b));
	else if (ft_strequ("rrr", cmd))
	{
		rrotate_stack(STCK(a));
		rrotate_stack(STCK(b));
	}
	else
		return (0);
	return (1);
}

int		ft_exec(t_env *p_env, char *cmd)
{
	if (!ft_exec_s(ARGS) && !ft_exec_p(ARGS)
		&& !ft_exec_r(ARGS) && !ft_exec_rr(ARGS))
		return (0);
	return (1);
}
