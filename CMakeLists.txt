cmake_minimum_required(VERSION 3.6)
project(push_swap)

set(CMAKE_C_STANDARD 99)

set(CMAKE_C_FLAGS "-Wall -Wextra -Werror")

set(SOURCE_FILES_ENV
		src/env/env.c
		src/env/ft_error.c
		src/env/ft_usage.c
		src/env/helpers.c
		src/env/parse_argv.c)

set(SOURCE_FILES_STACK
		src/stack/order.c
		src/stack/push.c
		src/stack/rotate.c
		src/stack/shell_sort.c
		src/stack/stack.c
		src/stack/stack_methods.c
		src/stack/swap.c
		src/stack/min_max.c
		src/stack/indexof.c
		src/stack/med.c)

set(SOURCE_FILES
		${SOURCE_FILES_STACK}
		${SOURCE_FILES_ENV})

set(SOURCE_FILES_CH
		src/checker/checker.c
		src/checker/ft_check.c
		src/checker/ft_exec.c)

set(SOURCE_FILES_PS
		src/push_swap/push_swap.c
		src/push_swap/sort_old.c
		src/push_swap/ft_p.c
		src/push_swap/ft_s.c
		src/push_swap/ft_r.c
		src/push_swap/ft_rr.c
		src/push_swap/ft_sort.c src/push_swap/ft_sortn.c)

add_subdirectory(libft)
add_subdirectory(ft_printf)

add_executable(push_swap
		${SOURCE_FILES_PS} ${SOURCE_FILES})
add_executable(checker
		${SOURCE_FILES_CH} ${SOURCE_FILES})

include_directories(${CMAKE_CURRENT_SOURCE_DIR})

set(FT_LIBRARIES
		ft ftprintf)

target_link_libraries(push_swap ${FT_LIBRARIES})
target_link_libraries(checker ${FT_LIBRARIES})