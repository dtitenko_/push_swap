#LIBFT  

##CONVERTERS  
|Function									|Description|
|:-----------------------------------------|-----------|
|`int ft_abs(int nb)`						|The `ft_abs()` function computes the absolute value of the integer `i`|
|`int ft_atoi(const char *str)`				|The `ft_atoi()` function converts the initial portion of the string pointed to by str to int representation|
|`char *ft_itoa(int n)`						|Converts an integer value to a null-terminated string using the base 10|
|`char *ft_ltoa_base(int value, int base)`	|Converts an integer value to a null-terminated string using the specified base|
|`int ft_toupper(int c)`					|The `ft_toupper()` function converts a lower-case letter to the corresponding upper-case letter.  The argument must be representable as an unsigned char or the value of EOF|
|`int ft_tolower(int c)`					|The `ft_tolower()` function converts a upper-case letter to the corresponding lower-case letter.  The argument must be representable as an unsigned char or the value of EOF|
|`int ft_wctomb(wchar_t wchr, ch  ar *str)`	|The `ft_wctomb()` function converts a wide character, `wchar`, into a multibyte character and stores the result in `str`|
|`int get_size_for_base(int n, int base)`	|The `get_size_for_base()` function counts the number of chars for `n` in base `base`|
|`char get_base_char(int i)`				|The `get_base_char()` returns character for `n`(`0 < n < 37`)|