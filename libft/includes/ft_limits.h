/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_limits.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/28 19:22:04 by dtitenko          #+#    #+#             */
/*   Updated: 2017/01/28 19:22:34 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LIMITS_H
# define FT_LIMITS_H

# define C_2			1
# define C_SIGN			1
# define I_LONG			0
# define MB_MAX			8

# define CHAR_BIT		8
# if C_SIGN
#  define CHAR_MAX		127
#  define CHAR_MIN		(-127 - C_2)
# else
#  define CHAR_MAX		255
#  define CHAR_MIN		0
# endif

# if I_LONG
#  define INT_MAX		2147483647
#  define INT_MIN		(-2147483647 - C_2)
#  define UINT_MAX		4294967295U
# else
#  define INT_MAX		32767
#  define INT_MIN		(-32767 - C_2)
#  define UINT_MAX		65535U
# endif

# define LONG_MAX		2147483647
# define LONG_MIN		(-2147483647 - C_2)

# define MB_LEN_MAX		MB_MAX

# define SCHAR_MAX		127
# define SCHAR_MIN		(-127 - C_2)

# define SHRT_MAX		32767
# define SHRT_MIN		(-32767 - C_2)

# define UCHAR_MAX		255
# define ULONG_MAX		4294967295
# define USHRT_MAX		65535U

#endif
