/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putlnbr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/26 17:43:16 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/26 17:59:59 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

static long int	ft_labs(long int n)
{
	if (n < 0)
		return (-n);
	return (n);
}

void			ft_putlnbr(long int n)
{
	char	a;

	if (n < 0)
		write(1, "-", 1);
	if (n / 10)
		ft_putlnbr(ft_labs(n / 10));
	a = '0' + ft_labs(n % 10);
	write(1, &a, 1);
}
