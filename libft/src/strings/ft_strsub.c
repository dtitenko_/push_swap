/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/01 18:33:43 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/02 20:39:11 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*ss;

	if (!s)
		return (NULL);
	if (ft_strlen(s) < start)
		return (NULL);
	if (!(ss = ft_strnew(len)))
		return (NULL);
	ft_strncpy(ss, s + start, len);
	return (ss);
}
