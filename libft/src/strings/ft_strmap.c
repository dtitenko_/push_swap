/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 19:30:26 by dtitenko          #+#    #+#             */
/*   Updated: 2016/11/30 19:48:02 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*fs;
	size_t	len;

	if (!s)
		return (NULL);
	len = ft_strlen(s);
	if (NULL == (fs = ft_strnew(len)))
		return (NULL);
	while (*s)
		*fs++ = f(*s++);
	return (fs - len);
}
