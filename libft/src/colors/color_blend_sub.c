/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color_blend_sub.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/26 17:45:33 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/26 17:46:26 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_colors.h>

t_color				color_blend_sub(t_color a, t_color b)
{
	t_color			c;
	int				red;
	int				green;
	int				blue;

	red = (int)a.argb.r * (int)a.argb.a - (int)b.argb.r * (int)b.argb.a;
	green = (int)a.argb.g * (int)a.argb.a - (int)b.argb.g * (int)b.argb.a;
	blue = (int)a.argb.b * (int)a.argb.a - (int)b.argb.b * (int)b.argb.a;
	if ((int)a.argb.a + (int)b.argb.a > 0)
		c.argb.a = ((a.argb.a > b.argb.a) ? a.argb.a : b.argb.a);
	else
		c.argb.a = 0;
	if (red > 0)
		c.argb.r = (unsigned char)(red / 255);
	else
		c.argb.r = 0;
	if (green > 0)
		c.argb.g = (unsigned char)(green / 255);
	else
		c.argb.g = 0;
	if (blue > 0)
		c.argb.b = (unsigned char)(blue / 255);
	else
		c.argb.b = 0;
	return (c);
}
