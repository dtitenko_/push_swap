/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fade_color.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/26 17:45:33 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/26 17:46:32 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_colors.h>

t_color				fade_color(t_color a, float opacity)
{
	a.argb.a = (unsigned char)((float)a.argb.a * opacity);
	return (a);
}
