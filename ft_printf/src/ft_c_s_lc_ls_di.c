/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_c_s_C_S_di.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/09 23:13:49 by dtitenko          #+#    #+#             */
/*   Updated: 2017/04/09 23:13:53 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/ft_printf.h"
#include "includes/ft_strings.h"
#include "includes/ft_convert.h"
#include <stdlib.h>
#define CHECK(prec, n1)	(prec > 0 && prec < n1)

void	ft_s(t_format *px, va_list pap, char code)
{
	int		pad;

	if (code == 's')
	{
		if ((*px).qual != 'l')
		{
			(*px).s = va_arg(pap, char *);
			if (!((*px).s))
			{
				(*px).s = "(null)";
			}
			(*px).n1 = (int)ft_strlen((*px).s);
			if (0 <= (*px).prec && (*px).prec < (*px).n1)
				(*px).n1 = (*px).prec;
			if (((*px).flags & (FZE | FMI)) == FZE
				&& 0 < (pad = (*px).width - (*px).n0 - (*px).nz0 - (*px).n1))
				(*px).nz0 += pad;
		}
		else
			ft_ls(px, pap, 'S');
	}
}

void	ft_c(t_format *px, va_list pap, char code, char *ac)
{
	int		pad;

	if (code == 'c')
	{
		if ((*px).qual != 'l')
		{
			(*px).s = &ac[(*px).n0];
			(*px).s[(*px).n1++] = va_arg(pap, int);
			pad = 0;
			if (((*px).flags & (FZE | FMI)) == FZE
				&& 0 < (pad = (*px).width - (*px).n0 - (*px).nz0 - (*px).n1))
				(*px).nz0 += pad;
		}
		else
			ft_lc(px, pap, 'C');
	}
}

void	ft_ls(t_format *px, va_list pap, char code)
{
	wchar_t	*ws;
	char	*mbs;
	size_t	len;
	int		pad;

	if (code == 'S')
	{
		ws = va_arg(pap, wchar_t *);
		if (!(ws))
		{
			mbs = ft_strdup("(null)");
			len = ft_strlen(mbs);
		}
		else
			len = CHECK((*px).prec, (*px).n1) ? ft_wcstombs(&mbs, ws) :
				ft_wcsntombs(&mbs, ws, (size_t)(*px).prec);
		if (len == (size_t)-1)
			return ;
		(*px).n1 = (int)len;
		(*px).s = mbs;
		if (((*px).flags & (FZE | FMI)) == FZE
			&& 0 < (pad = (*px).width - (*px).n0 - (*px).nz0 - (*px).n1))
			(*px).nz0 += pad;
	}
}

void	ft_lc(t_format *px, va_list pap, char code)
{
	wchar_t	wc;
	int		pad;

	if (code == 'C')
	{
		(*px).s = ft_strnew(4);
		wc = va_arg(pap, wchar_t);
		(*px).n1 = ft_wctomb((*px).s, wc);
		pad = 0;
		if (((*px).flags & (FZE | FMI)) == FZE
			&& 0 < (pad = (*px).width - (*px).n0 - (*px).nz0 - (*px).n1))
			(*px).nz0 += pad;
	}
}

void	ft_di(t_format *px, va_list pap, char code, char *ac)
{
	if (ft_strchr("di", code))
	{
		if ((*px).qual == 'l')
			(*px).v.li = ((*px).qual1 == 'l') ?
						va_arg(pap, long long) : va_arg(pap, long);
		else if ((*px).qual == 'h')
			(*px).v.li = ((*px).qual1 == 'h') ?
						(char)va_arg(pap, int) : (short)va_arg(pap, int);
		else if ((*px).qual == 'j' || (*px).qual == 'z')
			(*px).v.li = ((*px).qual1 == 'j') ? va_arg(pap, intmax_t)
											: va_arg(pap, ssize_t);
		else
			(*px).v.li = va_arg(pap, int);
		if ((long long)(*px).v.li < 0)
			ac[(*px).n0++] = '-';
		else if ((*px).flags & FPL)
			ac[(*px).n0++] = '+';
		else if ((*px).flags & FSP)
			ac[(*px).n0++] = ' ';
		(*px).s = &ac[(*px).n0];
		ft_litob(px, code);
	}
}
