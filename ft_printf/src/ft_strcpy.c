/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 03:43:57 by dtitenko          #+#    #+#             */
/*   Updated: 2016/11/26 19:24:36 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/ft_strings.h"

char	*ft_strcpy(char *dst, const char *src)
{
	char *dd;

	dd = dst;
	while (0 != (*dd++ = *src++))
		;
	return (dst);
}
