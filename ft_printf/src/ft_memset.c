/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 16:38:59 by dtitenko          #+#    #+#             */
/*   Updated: 2016/11/26 19:24:44 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/ft_memory.h"

void	*ft_memset(void *s, int c, size_t n)
{
	t_uchar	*ss;

	ss = (t_uchar *)s;
	while (n--)
		*ss++ = (t_uchar)c;
	return (s);
}
