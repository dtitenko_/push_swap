/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_powf.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 14:12:44 by dtitenko          #+#    #+#             */
/*   Updated: 2017/02/01 14:12:51 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

long double		ft_powf(double base, int exp)
{
	long double t;

	if (exp == 0)
		return (1);
	t = ft_powf(base, exp / 2);
	if (!(exp % 2))
		return (t * t);
	if (exp < 0)
		return (1 / base * t * t);
	return (base * t * t);
}
