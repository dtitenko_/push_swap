# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/08/14 17:37:32 by dtitenko          #+#    #+#              #
#    Updated: 2017/08/14 17:37:37 by dtitenko         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc
RM = /bin/rm
MKDIR = /bin/mkdir
PRINTF = /usr/bin/printf
ECHO = /bin/echo
CFLAGS = -Wall -Wextra -Werror
PUSH_SWAP_BIN = push_swap
CHECKER_BIN = checker
NAME=push_swap

LIBFT_DIR = libft
FT_PRINTF_DIR = ft_printf

IFLAGS = -I.
LFLAGS = -L$(LIBFT_DIR) -lft -L$(FT_PRINTF_DIR) -lftprintf

SRC_DIR = src/
OBJ_DIR = obj/

PS_DIR = push_swap/
CH_DIR = checker/
ENV_DIR = env/
STCK_DIR = stack/

PS_DIR_OBJ = $(addprefix $(OBJ_DIR),$(PS_DIR))
CH_DIR_OBJ = $(addprefix $(OBJ_DIR),$(CH_DIR))
ENV_DIR_OBJ = $(addprefix $(OBJ_DIR),$(ENV_DIR))
STCK_DIR_OBJ = $(addprefix $(OBJ_DIR),$(STCK_DIR))

PS_SRC =	ft_p.c			ft_r.c		ft_rr.c			ft_s.c \
			ft_sort.c		push_swap.c	sort_old.c		ft_sortn.c

CH_SRC =	checker.c		ft_check.c	ft_exec.c

ENV_SRC =	ft_error.c		ft_usage.c	helpers.c		parse_argv.c \
			env.c

STCK_SRC =	indexof.c		med.c		min_max.c		order.c \
			push.c			rotate.c	shell_sort.c	stack.c \
			stack_methods.c	swap.c

PS_SRC := $(addprefix $(PS_DIR),$(PS_SRC))
CH_SRC := $(addprefix $(CH_DIR),$(CH_SRC))
ENV_SRC := $(addprefix $(ENV_DIR),$(ENV_SRC))
STCK_SRC := $(addprefix $(STCK_DIR),$(STCK_SRC))


PS_OBJ = $(PS_SRC:.c=.o)
CH_OBJ = $(CH_SRC:.c=.o)
ENV_OBJ = $(ENV_SRC:.c=.o)
STCK_OBJ = $(STCK_SRC:.c=.o)

PUSH_SWAP_OBJ = $(PS_OBJ) $(ENV_OBJ) $(STCK_OBJ)
CHECKER_OBJ = $(CH_OBJ) $(ENV_OBJ) $(STCK_OBJ)
PUSH_SWAP_OBJ := $(addprefix $(OBJ_DIR),$(PUSH_SWAP_OBJ))
CHECKER_OBJ := $(addprefix $(OBJ_DIR),$(CHECKER_OBJ))

# Colors

NO_COLOR =		\033[0;00m
OK_COLOR =		\033[38;5;02m
ERROR_COLOR =	\033[38;5;01m
WARN_COLOR =	\033[38;5;03m
SILENT_COLOR =	\033[38;5;04m

.PHONY: all mkdir_obj mkdir_ps mkdir_ch mkdir_env mkdir_stck libft ft_printf \
		stack env ps ch

all: $(PUSH_SWAP_BIN) $(CHECKER_BIN)
	@$(PRINTF) "$(OK_COLOR)Successful ✓$(NO_COLOR)\n"


$(PUSH_SWAP_BIN): mkdir_ps mkdir_stck mkdir_env libft ft_printf stack env ps
	@$(PRINTF) "$(SILENT_COLOR)$(PUSH_SWAP_BIN) binary$(NO_COLOR)"
	@$(CC) $(IFLAGS) $(CFLAGS) $(PUSH_SWAP_OBJ) $(LFLAGS) -o $@
	@$(PRINTF)		"\t[$(OK_COLOR)✓$(NO_COLOR)]$(NO_COLOR)\n"

$(CHECKER_BIN): mkdir_ch mkdir_stck mkdir_env libft ft_printf stack env ch
	@$(PRINTF) "$(SILENT_COLOR)$(CHECKER_BIN) binary$(NO_COLOR)"
	@$(CC) $(IFLAGS) $(CFLAGS) $(CHECKER_OBJ) $(LFLAGS) -o $@
	@$(PRINTF) "\t\t[$(OK_COLOR)✓$(NO_COLOR)]$(NO_COLOR)\n"

$(OBJ_DIR)%.o: $(SRC_DIR)%.c
	@$(CC) $(FLAGS) $(IFLAGS) -o $@ -c $<
	@$(PRINTF) "$(OK_COLOR)✓ $(NO_COLOR)$<\n"


stack: $(addprefix $(OBJ_DIR),$(STCK_OBJ))

env: $(addprefix $(OBJ_DIR),$(ENV_OBJ))

ps: $(addprefix $(OBJ_DIR),$(PS_OBJ))

ch: $(addprefix $(OBJ_DIR),$(CH_OBJ))

libft:
	@make -C $(LIBFT_DIR) all
	@$(ECHO) ""

ft_printf:
	@make -C $(FT_PRINTF_DIR) all
	@$(ECHO) ""

mkdir_obj:
	@$(MKDIR) -p $(OBJ_DIR)

mkdir_env: mkdir_obj
	@$(MKDIR) -p $(ENV_DIR_OBJ)

mkdir_stck: mkdir_obj
	@$(MKDIR) -p $(STCK_DIR_OBJ)

mkdir_ps: mkdir_obj
	@$(MKDIR) -p $(PS_DIR_OBJ)

mkdir_ch: mkdir_obj
	@$(MKDIR) -p $(CH_DIR_OBJ)

clean:
	@$(RM) -rf $(CH_DIR_OBJ)
	@$(PRINTF) "$(SILENT_COLOR)$(NAME) : $(PS_DIR_OBJ) : Cleaned Objects$(NO_COLOR)\n"
	@$(RM) -rf $(CH_DIR_OBJ)
	@$(PRINTF) "$(SILENT_COLOR)$(NAME) : $(CH_DIR_OBJ) : Cleaned Objects$(NO_COLOR)\n"
	@$(RM) -rf $(ENV_DIR_OBJ)
	@$(PRINTF) "$(SILENT_COLOR)$(NAME) : $(ENV_DIR_OBJ) : Cleaned Objects$(NO_COLOR)\n"
	@$(RM) -rf $(STCK_DIR_OBJ)
	@$(PRINTF) "$(SILENT_COLOR)$(NAME) : $(STCK_DIR_OBJ) : Cleaned Objects$(NO_COLOR)\n"
	@$(RM) -rf $(OBJ_DIR)
	@$(PRINTF) "$(SILENT_COLOR)$(NAME) : $(OBJ_DIR) : Cleaned Objects$(NO_COLOR)\n"
	@make -s -C $(LIBFT_DIR) clean
	@make -s -C $(FT_PRINTF_DIR) clean

fclean: clean
	@$(RM) -f $(PUSH_SWAP_BIN)
	@$(PRINTF) "$(SILENT_COLOR)$(NAME) : ./$(PUSH_SWAP_BIN) : Cleaned binary$(NO_COLOR)\n"
	@$(RM) -f $(CHECKER_BIN)
	@$(PRINTF) "$(SILENT_COLOR)$(NAME) : ./$(CHECKER_BIN) : Cleaned binary$(NO_COLOR)\n"
	@make -s -C $(LIBFT_DIR) fclean
	@make -s -C $(FT_PRINTF_DIR) fclean

re: fclean all
