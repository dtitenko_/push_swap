/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   structs.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:35:22 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:35:23 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_STRUCTS_H
# define PUSH_SWAP_STRUCTS_H

typedef struct	s_stack
{
	int			top;
	int			maxlen;
	char		ch;
	int			*arr;
}				t_stack;

typedef	struct	s_env
{
	t_stack		*a;
	t_stack		*b;
	char		*buf;
	int			flags;
	int			fd;
}				t_env;

#endif
