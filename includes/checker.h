/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:34:43 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:34:45 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_CHECKER_H
# define PUSH_SWAP_CHECKER_H

# include <stdlib.h>
# include <unistd.h>

# include "env.h"
# include "stack.h"

int		ft_exec(t_env *p_env, char *cmd);
int		ft_check(t_env *p_env);

#endif
