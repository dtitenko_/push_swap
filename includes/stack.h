/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:35:14 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:35:15 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_STACK_H
# define PUSH_SWAP_STACK_H

# include "structs.h"
# include <stdlib.h>

# define STCK(x) (p_env->x)

/*
** stack.c
*/
void			ft_free_stack(t_stack **p_stack);
t_stack			*ft_init_stack(int maxlen, char ch);
t_stack			*ft_fill_stack(int argc, char **argv, int i, t_env **pp_env);

/*
** stack_methods.c
*/
int				isfull(t_stack *p_stack);
int				isempty(t_stack *p_stack);
int				instack(t_stack *p_stack, int val);
long long		push(t_stack *p_stack, int data);
long long		pop(t_stack *p_stack, int *data);

/*
** order.c
*/
int				ft_check_order(t_stack *p_stack, int desc);

/*
** min_max.c
*/
int				ft_max_in_stack(t_stack *p_stack);
int				ft_min_in_stack(t_stack *p_stack);

/*
** med.c
*/
int				ft_med_in_stack(t_stack *p_stack, int sz);

/*
** indexof.c
*/
int				ft_stack_indexof(int val, t_stack *p_stack);

/*
** shell_sort.c (necessary for median)
*/
void			shell_sort(int *arr, size_t size, int desc);

/*
** stack operations
*/
void			rotate_stack(t_stack *p_stack);
void			rrotate_stack(t_stack *p_stack);
void			swap_stack(t_stack *p_stack);
void			push_stack(t_stack *dst, t_stack *src);

#endif
