/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:35:00 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:35:01 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_PUSH_SWAP_H
# define PUSH_SWAP_PUSH_SWAP_H

# include <unistd.h>
# include <stdlib.h>

# include "env.h"
# include "stack.h"

/*
** All sources to implement and exec main sorting algorithm for push_swap.
** Folder with sources: src/push_swap/;
** Source files:
**     push_swap.c, sort.c
**     ft_s.c, ft_p.c, ft_r.c, ft_rr.c;
*/

/*
** Binary flags to set current stack.
*/
# define A 0x01
# define B 0x02

/*
** Macro to make shorter access to stacks and elements in
** (e.g. TOP_INDX(a), TOP(b)).
*/
# define TOP_INDX(x) p_env->x->top
# define ELEM(x, pos) p_env->x->arr[pos]
# define TOP(x) ELEM(x, p_env->x->top)
# define NEXT2TOP(x) ELEM(x, p_env->x->top - 1)
# define BOTTOM(x) ELEM(x, 0)
# define PREV2BOTTOM(x) ELEM(x, 1)
# define ROT2(x) (TOP_INDX(x) / 2 <= ft_stack_indexof(tmp, p_env->x))

/*
** Macro to access to stack elements by pointer passed as argument.
** Example: S_TI(p_env->a), S_ELEM(p_env->b)...
*/
# define S_TI(x) (x->top)
# define S_ELEM(x, pos) (x->arr[pos])
# define S_TOP(x) S_ELEM(x, S_TI(x))
# define S_N2T(x) S_ELEM(x, S_TI(x) - 1)
# define S_3D(x) S_ELEM(x, S_TI(x) - 2)
# define S_4TH(x) S_ELEM(x, S_TI(x) - 3)
# define S_BOT(x) S_ELEM(x, 0)
# define S_P2B(x) S_ELEM(x, 1)
# define S_ROT2(x) (S_TI(x) / 2 <= ft_stack_indexof(tmp, x))

/*
** Macro to find out which stack is the current one.
*/
# define ONLY_A ((stack & (A | B)) == A)
# define ONLY_B ((stack & (A | B)) == B)
# define BOTH (stack & (A | B))
# define IF_A_ELSE_B(a, b) (ONLY_A ? a : b)

/*
** Macro to access to elements in current stack.
** C*, C_*   - current stack;
** NC*, NC_* - non current stack;
*/
# define C_STACK (ONLY_A ? STCK(a) : STCK(b))
# define C_TI S_TI(C_STACK)
# define C_ELEM(pos) S_ELEM(C_STACK, pos)
# define C_TOP S_TOP(C_STACK)
# define C_N2T S_N2T(C_STACK)
# define C_3D S_3D(C_STACK)
# define C_4TH S_4TH(C_STACK)
# define C_BOT S_BOT(C_STACK)
# define C_P2B S_P2B(C_STACK)
# define C_ROT2 S_ROT2(C_STACK)

# define NC_STACK (ONLY_A ? STCK(b) : STCK(a))
# define NC_TI S_TI(NC_STACK)
# define NC_ELEM(pos) S_ELEM(NC_STACK, pos)
# define NC_TOP S_TOP(NC_STACK)
# define NC_N2T S_N2T(NC_STACK)
# define NC_3D S_3D(NC_STACK)
# define NC_4TH S_4TH(NC_STACK)
# define NC_BOT S_BOT(NC_STACK)
# define NC_P2B S_P2B(NC_STACK)
# define NC_ROT2 S_ROT2(NC_STACK)

/*
** Macro to perform operations on the right stack.
*/
# define CSTACK_SWAP (IF_A_ELSE_B(ft_sa(p_env), ft_sb(p_env)))
# define CSTACK_PUSH (IF_A_ELSE_B(ft_pb(p_env), ft_pa(p_env)))
# define CSTACK_ROT (IF_A_ELSE_B(ft_ra(p_env), ft_rb(p_env)))
# define CSTACK_RROT (IF_A_ELSE_B(ft_rra(p_env), ft_rrb(p_env)))

# define NCSTACK_SWAP (IF_A_ELSE_B(ft_sb(p_env), ft_sa(p_env)))
# define NCSTACK_PUSH (IF_A_ELSE_B(ft_pa(p_env), ft_pb(p_env)))
# define NCSTACK_ROT (IF_A_ELSE_B(ft_rb(p_env), ft_ra(p_env)))
# define NCSTACK_RROT (IF_A_ELSE_B(ft_rrb(p_env), ft_rra(p_env)))

/*
** Macro to check reverse order and change solution to opposite.
*/
# define R (p_env->flags & F_REV)
# define CHECK(eq) (R ? !(eq) : (eq))

/*
** C_ - current stack
*/
# define REV ((ONLY_A && R) || (ONLY_B && !R))
# define MINMEDMAX (C_TOP < C_N2T && C_N2T < C_3D && C_3D > C_TOP)
# define MINMAXMED (C_TOP < C_N2T && C_N2T > C_3D && C_3D > C_TOP)
# define MEDMINMAX (C_TOP > C_N2T && C_N2T < C_3D && C_3D > C_TOP)
# define MEDMAXMIN (C_TOP < C_N2T && C_N2T > C_3D && C_3D < C_TOP)
# define MAXMINMED (C_TOP > C_N2T && C_N2T < C_3D && C_3D < C_TOP)
# define MAXMEDMIN (C_TOP > C_N2T && C_N2T > C_3D && C_3D < C_TOP)
# define MAXMIN (C_TOP > C_N2T)
# define MINMAX (C_TOP < C_N2T)

/*
** Possible permutations of three elements
** T - Element that should be on top;
** M - Element that should stand in the middle;
** B - Element that should stand at the bottom;
*/
# define C_TMB (REV ? MAXMEDMIN : MINMEDMAX)
# define C_TBM (REV ? MAXMINMED : MINMAXMED)
# define C_MTB (REV ? MEDMAXMIN : MEDMINMAX)
# define C_MBT (REV ? MEDMINMAX : MEDMAXMIN)
# define C_BTM (REV ? MINMAXMED : MAXMINMED)
# define C_BMT (REV ? MINMEDMAX : MAXMEDMIN)

/*
** Possible permutations  of 2 elements
*/
# define C_TB (REV ? MAXMIN : MINMAX)
# define C_BT (REV ? MINMAX : MAXMIN)

/*
** Macro to check if max(or min if R) is 4th from top
*/
# define MAX4TH (C_4TH > C_TOP && C_4TH > C_N2T && C_4TH > C_3D)
# define MIN4TH (C_4TH < C_TOP && C_4TH < C_N2T && C_4TH < C_3D)
# define C_M4TH (REV ? MIN4TH : MAX4TH)

/*
** Old algorithm
*/
int		ft_check_n_swap_last(t_env *p_env);
int		ft_check_n_swap_first(t_env *p_env);
void	ft_sort_old(t_env *p_env);

/*
** New algorithm
*/
void	ft_sort(t_env *p_env, char stack, int sz);
void	ft_sort3(t_env *p_env, char stack);
void	ft_sort3_top_a(t_env *p_env, char stack);
void	ft_sort3_top_b(t_env *p_env, char stack);
void	ft_sort4_max(t_env *p_env, char stack);
void	ft_push2a(t_env *p_env, char stack, int n);

/*
** Wrappers for swaps (ft_s.c)
*/
void	ft_sa(t_env *p_env);
void	ft_sb(t_env *p_env);
void	ft_ss(t_env *p_env);

/*
** Wrappers for pushes (ft_p.c)
*/
void	ft_pa(t_env *p_env);
void	ft_pb(t_env *p_env);

/*
** Wrappers for rotations (ft_r.c)
*/
void	ft_ra(t_env *p_env);
void	ft_rb(t_env *p_env);
void	ft_rr(t_env *p_env);

/*
** Wrappers for reverse rotations (ft_rr.c)
*/
void	ft_rra(t_env *p_env);
void	ft_rrb(t_env *p_env);
void	ft_rrr(t_env *p_env);

#endif
