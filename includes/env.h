/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 17:34:54 by dtitenko          #+#    #+#             */
/*   Updated: 2017/08/14 17:34:54 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_ENV_H
# define PUSH_SWAP_ENV_H

# include "libft/includes/libft.h"
# include "includes/structs.h"

# define F_VERBOSE		0x01
# define F_COLOR		0x02
# define F_REV			0x04
# define F_HELP			0x08
# define F_OLDALG		0x10
# define PARSE_ERROR	0x20

/*
** env.c
*/
t_env			*ft_envinit(void);
void			ft_envdel(t_env **env);

/*
** messages
*/
void			ft_usage(int argc, char **argv);
void			ft_print_error(t_env **env);

/*
** parse_argv.c
*/
int				parse_argv(int argc, char **argv, int *stop);
int				ft_isallnum(char *str);

/*
** helpers.c
*/
void			ft_verbose(t_env *p_env, char *cmd);
void			push2buf(t_env *p_env, char *cmd);
void			print_stacks(t_stack *a, t_stack *b);

#endif
